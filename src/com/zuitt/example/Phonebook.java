package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {
    //-------------instance variable-----------------//
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    //-------------default constructor---------------//
    public Phonebook() {};

    //-----------parametrized constructor------------//
    public Phonebook(Contact contact) {
        this.contacts.add(contact);
    }

    //---------------getter----------------//
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    //---------------setter----------------//
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }
}
