package com.zuitt.example;

public class Main {

    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();

        contact1.setName("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("my home in Quezon City");

        Contact contact2 = new Contact("Jane Doe","+639162148573","my home in Caloocan City");


        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().isEmpty()){
            System.out.println("The phonebook is currently empty.");
        }else{

            phonebook.getContacts().forEach((contact) -> printInfo(contact));
        }
    }


    public static void printInfo(Contact contact){
        System.out.println(contact.getName());
        System.out.println("-------------------");
        if(contact.getContactNumber().isEmpty()){
            System.out.println(contact.getName() + " has no registered number.");
        }else{
            System.out.println(contact.getName() + " has the following registered number:");
            System.out.println(contact.getContactNumber());
        }
        if(contact.getAddress().isEmpty()){
            System.out.println(contact.getName() + " has no registered address.");
        }else{
            System.out.println(contact.getName() + " has the following registered address:");
            System.out.println(contact.getAddress());
        }
    }
}
